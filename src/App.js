import Input from './Components/Input';
import { Switch, BrowserRouter } from 'react-router-dom'
import Allemployees from './Components/Allemployees';
import Alpha from './Components/Alpha';


const App = () => {
  return <BrowserRouter>
    <Switch>
      <Allemployees exact path='/all' component={Allemployees} />
      <Alpha exact path='/alpha' component={Alpha} />
      <Input exact path='/' component={Input} />
    </Switch>
  </BrowserRouter>

  
}

export default App;

