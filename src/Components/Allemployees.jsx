import React from 'react'
import { Link } from 'react-router-dom'



const Allemployees = (props) => { 

const employee_names = props.location.aboutProps.nameList

return <div role="main">
<h1 className="title">Employee Data Manager</h1>
<section className="input_section">
<h2>Employee Summary</h2>
<table>
  <tbody>
  {employee_names.map((employee, index) => {
      return <tr key={index}>
        <td>{employee.name}</td>
        <td>{employee.email}</td>
      </tr>
    })}  
  </tbody>
</table>

<Link className="button" to={ { pathname:"/alpha", state: { employee_names } } }>Show Employees Alphabetically</Link>
</section>
</div>
}

export default Allemployees


