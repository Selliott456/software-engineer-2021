import React from 'react'

const Alpha = (props) => {
const nameList = props.location.state.employee_names
const newNameList = nameList.sort((a, b) => a.name.localeCompare(b.name))
return<div role="main">
<h1 className="title">Employee Data Manager</h1>
<section className="input_section">
<h2>Show employees alphabetically</h2>
<table>
  <tbody>
  {newNameList.map((employee, index) => {
      return <tr key={index}>
        <td>{employee.name}</td>
        <td>{employee.email}</td>
      </tr>
    })
    }
  </tbody>
</table>
</section>
</div>
}
export default Alpha