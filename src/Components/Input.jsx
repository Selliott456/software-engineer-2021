import React, { useState } from 'react'
import { Link } from 'react-router-dom'



const Home = () => {


  const [nameList, updateNameList] = useState([
    { name: "Zaynab", email: "zaynab@company.com" },
    { name: "Jonathan", email: "jonathan@company.com" },
    { name: "Mary", email: "mary@company.com" },
    { name: "Anne", email: "anne@company.com" }
  ])

  const [newPerson, updateNewPerson] = useState({
    name: '',
    email: '',
  })

  const [search, updateSearch] = useState('')
  const [filteredNames, updateFilteredNames] = useState([])

  function searchEmployees(event) {
    event.preventDefault()
    const input = search.toLowerCase()
    const new_arr = []
    if(input) {
    for (let i = 0; i < nameList.length; i++) {
      const person = nameList[i].name
      if (person.toLowerCase().includes(input)) {
        new_arr.push(nameList[i]);
      }
      updateFilteredNames(new_arr)
    }
  } else {
    return
  }



  }

  function handleChange(event) {
    const name = event.target.name
    const value = event.target.value
    const data = {
      ...newPerson,
      [name]: value
    }
    updateNewPerson(data)
  }

  function addEmployee(event) {
    event.preventDefault()
    const data = [...nameList, newPerson]
    updateNameList(data)
  }

  return (
    <>
      <main>
        <p id="test">Component here</p>
        <h1 className="title">Employee Data Manager</h1>
        <section className="input_section">
          <form className="input_field" onSubmit={searchEmployees}>
            <h2>Search for current employee</h2>
            <label>Enter Name: </label>
            <div className="add">
              <input
                placeholder="Enter Name"
                onChange={(event) => updateSearch(event.target.value)}
                value={search}
              ></input>
              <button>search</button>
            </div>
          </form>
          <table>
            <tbody>
              {filteredNames && filteredNames.map((employee, index) => {
                return <tr key={index}>
                  <td>{employee.name}</td>
                  <td>{employee.email}</td>
                </tr>
              })
              }
            </tbody>
          </table>
          <Link className="button" to={{ pathname: "/all", aboutProps: { nameList } }}>Show all Employees</Link>
        </section>
        <section className="input_section">
          <form className="input_field" onSubmit={addEmployee}>
            <h2>Add a new employee</h2>
            <div className="add_bar">
              <div className="add">
                <label>Employee Name: </label>
                <input
                  type="text"
                  placeholder="Enter Name"
                  onChange={handleChange}
                  value={newPerson.name}
                  name="name"
                ></input>
              </div>
              <div className="add">
                <label>Email: </label>
                <input
                  type="text"
                  placeholder="Enter Name"
                  onChange={handleChange}
                  value={newPerson.email}
                  name="email"
                ></input>
                <button>Add</button>
              </div>
            </div>
          </form>
        </section>
      </main>
    </>
  )
};

export default Home;